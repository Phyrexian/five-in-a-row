#include "library/lib.cpp"

Position choose_move(Board &board) {
    for(int x = 0; x < n; ++x)
			for(int y = 0; y < n; ++y)
				if(board(x, y) == Empty)
					return {x, y};
	return Position();
}

vector<int> dx = {-1, 1, 1, -1, 1, -1, 0, 0};
vector<int> dy = {-1, 1, -1, 1, 0, 0, 1, -1};

constexpr int inf = 1e9;
// returns how good the board is for me, not for the curr_player
int evaluation_fce(Board &board, vector<Position> &moves) {
	array<int, 5> f = {0, 1, 10, 200, 100000};
	auto check = [&](State player) {
		int sum = 0;
		// check for available moves
		for(Position &p : moves)
			for(int dir = 0; dir < 8; dir += 2) {
				int x = p.x, y = p.y;
				int _x = x, _y = y;
				int len = 0;

				do {
					_x += dx[dir];
					_y += dy[dir];
					len++;
				}
				while(board(_x, _y) == player);
				len--;

				_x = x, _y = y;
				do {
					_x += dx[dir + 1];
					_y += dy[dir + 1];
					len++;
				}
				while(board(_x, _y) == player);
				len--;

				len = min(len, 4);
				sum += f[len];
			}

		for(int x = 0; x < n; x++) {
			for(int y = 0; y < n; y++) {
				for(int dir = 0; dir < 8; dir += 2) {
					// check for win
					if(board(x, y) == player) {
						int _x = x, _y = y;
						int len = 0;
						do {
							_x += dx[dir];
							_y += dy[dir];
							len++;
						}
						while(board(_x, _y) == player);

						if(len >= 5) return inf;
					}
				}
			}
		}

		return sum;
	};

	return check(Me) - check(Enemy);
}

Clock minmax_time;
constexpr int max_depth = 2;
int minmax(Board &board, int depth = 0, bool maximize = true, int alpha = -inf, int beta = inf) {
	vector<Position> moves(board.moves.begin(), board.moves.end());
	if(depth == max_depth || elapsed_ms(minmax_time) >= 175)
		return evaluation_fce(board, moves);
	Position opt;
	int opt_score = (maximize ? -inf : inf);

	Board cp = board;
	shuffle(moves.begin(), moves.end(), rng);
	for(Position p : moves) {
		board.apply_move(p);

		int s = minmax(board, depth + 1, maximize ^ 1, alpha, beta);
		if((maximize && s > opt_score) || (!maximize && s < opt_score)) {
			opt_score = s;
			opt = p;
		}
		board.undo_move(p);
		board.moves = set<Position>(moves.begin(), moves.end());

		if(maximize) alpha = max(alpha, opt_score);
		else beta = min(beta, opt_score);
		if(alpha >= beta) break;
	}
	assert(cp.board == board.board and cp.moves == board.moves);

	if(depth == 0)
		board.apply_move(opt, true);
	return opt_score;
}

int main() {
	for(int i = 0; i < 3; ++i) {
		int ignore_input;
		cin >> ignore_input;
		if(i != 2)
			assert(ignore_input == n);
	}

	Board curr_state;
	curr_state.curr_player = Enemy;
	curr_state.recalculate_available_moves();

	int enemy_x, enemy_y;
	while(cin >> enemy_y >> enemy_x) {
		minmax_time = new_clock();
		if(enemy_x == -1 and enemy_y == -1) // first move was opponent's
			curr_state.curr_player = Me;
		else
			curr_state.apply_move({enemy_x, enemy_y});
		curr_state.recalculate_available_moves();

		minmax(curr_state);
    }
}
