#include "templates.cpp"

enum State {
	Me = 0,
	Enemy = 1,
	Empty = 2
};

struct Position {
	int x, y;
	Position(int a = -1, int b = -1) : x(a), y(b) {}
};

bool operator==(Position a, Position b) {
	return a.x == b.x and a.y == b.y;
}

bool operator<(Position a, Position b) {
	if(a.y != b.y)
		return a.y < b.y;
	return a.x < b.x;
}

ostream& operator<<(ostream &out, const Position &p) {
	return out << '(' << p.x << ", " << p.y << ')';
}

constexpr int n = 25;

struct Board {
	int curr_player = 0;
	array<array<State, n>, n> board;
	set<Position> moves;

	Board() {
		for(int x = 0; x < n; ++x)
			fill(board[x].begin(), board[x].end(), Empty);
	}

	void apply_move(Position p, bool print = false) {
		if(print)
			cout << p.y << ' ' << p.x << endl << flush;
		assert(board[p.x][p.y] == Empty);
		board[p.x][p.y] = State(curr_player);
		curr_player ^= 1;
		auto it = moves.find(p);
		if(it != moves.end())
			moves.erase(it);
		for(int dx : {-1, 0, 1})
			for(int dy : {-1, 0, 1})
				if((dx != 0 or dy != 0)
						and 0 <= p.x + dx and p.x + dx < n
						and 0 <= p.y + dy and p.y + dy < n
						and board[p.x + dx][p.y + dy] == Empty)
					moves.emplace(Position(p.x + dx, p.y + dy));
	}

	void undo_move(Position p) {
		// doesnt undo the moves set
		assert(board[p.x][p.y] != Empty);
		board[p.x][p.y] = Empty;
		curr_player ^= 1;
	}

	State operator()(Position p) {
		return (*this)(p.x, p.y);
	}

	State operator()(int x, int y) {
		if(0 <= x and x < n and 0 <= y and y < n)
			return board[x][y];
		return Empty;
	}

	/*State winning_state() {
		for(int x = 0; x < n; ++x)
			for(int y = 0; y < n; ++y)
				for(int dx : {0, 1})
					for(int dy : {0, 1})
						if(dx != 0 or dy != 0)
							if(x + 4 * dx < n - 1 and y + 4 * dy < n - 1) {
								State first = board[x][y];
								bool failed = false;
								for(int k = 1; k <= 4; ++k)
									if(first != board[x + k * dx][y + k * dy]) {
										failed = true;
										break;
									}
								if(not failed)
									return first;
							}
		return Empty;
	}*/

	void recalculate_available_moves() {
		moves.clear();
		for(int x = 0; x < n; ++x)
			for(int y = 0; y < n; ++y) {
				if(board[x][y] != Empty)
					continue;
				bool any_nearby = false;
				for(int dx : {-1, 0, 1})
					for(int dy : {-1, 0, 1})
						if((dx != 0 or dy != 0)
								and 0 <= x + dx and x + dx < n
								and 0 <= y + dy and y + dy < n)
							if(board[x + dx][y + dy] != Empty) {
								any_nearby = true;
								break;
							}
				if(any_nearby)
					moves.emplace(Position(x, y));
			}
		if(size(moves) == 0) {
			moves = {Position(n / 2, n / 2)};
		}
	}
};

ostream& operator<<(ostream &os, Board b) {
	os << "[\n";
	for(int y = 0; y < n; ++y) {
		for(int x = 0; x < n; ++x)
			if(b(x, y) == Empty)
				os << '.';
			else
				os << b(x, y);
		os << '\n';
	}
	return os << ']';
}

using Clock = decltype(chrono::high_resolution_clock::now());

inline int elapsed_ms(Clock &start) {
	Clock end = chrono::high_resolution_clock::now();
	int milli = int(chrono::duration_cast<chrono::milliseconds>(end - start).count());
	return milli;
}

inline Clock new_clock() {
	return chrono::high_resolution_clock::now();
}

