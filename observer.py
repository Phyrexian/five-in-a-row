import sys, pygame

width, height = 750, 750
r, c = 25, 25

def draw_cross(screen, i, j):
    pygame.draw.line(screen, (255, 0, 255), (j * width // c + 7, i * height // r + 7), ((j + 1) * width // c - 7, (i + 1) * height // r - 7), 4)
    pygame.draw.line(screen, (255, 0, 255), (j * width // c + 7, (i + 1) * height // r - 7), ((j + 1) * width // c - 7, i * height // r + 7), 4)

def draw_circle(screen, i, j):
    pygame.draw.circle(screen, (0, 255, 255), (int((2 * j + 1) * width / c / 2), int((2 * i + 1) * height / r / 2)), width // (2 * c) - 5, 3)
 
def main():
    global width, height, r, c
    moves = []
    plnames = []
    with open(sys.argv[1], 'r') as f:
        _, r, c = [ int(x) for x in f.readline().split(' ') ]
        plnames = f.readline().split(' vs ')
        plnames[1] = plnames[1].rstrip('\r\n')
        line = f.readline()
        while line:
            pr, pc = [ int(x) for x in line.split(' ') ]
            moves.append((pr, pc))
            line = f.readline()
    pygame.init()
    logo = pygame.image.load("tictactoe.jpg")
    pygame.display.set_icon(logo)
    pygame.display.set_caption("observer.py [ {} vs {} ]".format(plnames[0], plnames[1]))
    screen = pygame.display.set_mode((width + 1, height + 1))
    running, t = True, 0
    while running:
        pressed = pygame.key.get_pressed()
        alt_held = pressed[pygame.K_LALT] or pressed[pygame.K_RALT]
        ctrl_held = pressed[pygame.K_LCTRL] or pressed[pygame.K_RCTRL]
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_w and ctrl_held:
                    running = False
                if event.key == pygame.K_F4 and alt_held:
                    running = False
                if event.key == pygame.K_ESCAPE:
                    running = False
        screen.fill((0, 0, 0))
        for i in range(r + 1):
            pygame.draw.line(screen, (255, 255, 255), (0, i * height / r), (width + 1, i * height / r), 2)
        for i in range(c + 1):
            pygame.draw.line(screen, (255, 255, 255), (i * width / c, 0), (i * width / c, height + 1), 2)

        for i in range(min(t, len(moves))):
            if i % 2 == 0:
                draw_cross(screen, moves[i][0], moves[i][1])
            else:
                draw_circle(screen, moves[i][0], moves[i][1])
        pygame.display.flip()
        pygame.time.delay(500)
        t += 1
     
if __name__=="__main__":
    main()