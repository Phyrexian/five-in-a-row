import os, signal, sys, zipfile, shutil, subprocess, time, datetime, threading

def run(): 
    global proces, board, player, winner, moves, last_player, last_time
    while winner is -2:
        try:
            pr, pc = [ int(x) for x in proces[player].stdout.readline().decode().split() ]
        except:
            print("Player {} crashed".format(player))
            winner = player ^ 1
            break
        last_player, last_time = player, time.time()
        if winner is not -2:
            break
        if pr < 0 or pr >= r or pc < 0 or pc >= c or board[pr][pc] is not -1:
            print("Player {} played an invalid move {} {}".format(player, pr, pc))
            winner = player ^ 1
            break
        board[pr][pc] = player
        moves.append((pr, pc))
        if check_win(pr, pc):
            winner = player
        elif len(moves) is r * c:
            winner = -1
        else:
            proces[player^1].stdin.write("{} {}\n".format(pr, pc).encode())
            proces[player^1].stdin.flush()
            player ^= 1
    
print(sys.argv)
r, c = 15, 15
if len(sys.argv) > 3:
    r = int(sys.argv[3])
if len(sys.argv) > 4:
    c = int(sys.argv[4])
board = [[-1 for i in range(c)] for j in range(r)]

def check_win(pr, pc):
    dr = [ 0, -1, -1, -1, 0, 1, 1, 1 ]
    dc = [ -1, -1, 0, 1, 1, 1, 0, -1 ]
    cnt = [ 0, 0, 0, 0, 0, 0, 0, 0 ]
    for k in range(8):
        nr, nc = pr + dr[k], pc + dc[k]
        while nr >= 0 and nr < r and nc >= 0 and nc < c and board[nr][nc] == board[pr][pc]:
            cnt[k] += 1
            nr += dr[k]
            nc += dc[k]
    for k in range(4):
        if cnt[k] + cnt[k + 4] + 1 >= 5:
            return True
    return False

proces = [None] * 2
for i in range(2):
    os.system("cd {} && make".format(sys.argv[i + 1]))

for i in range(2):
    proces[i] = subprocess.Popen("cd {} && make -s run".format(sys.argv[i + 1]), shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    proces[i].stdin.write("{} {} {}\n".format(r, c, i).encode())
    proces[i].stdin.flush()

proces[0].stdin.write("{} {}\n".format(-1, -1).encode())
proces[0].stdin.flush()
player, winner, moves = 0, -2, []
last_player, last_time = 1, time.time()

my_thread = threading.Thread(target=run, daemon=True)
my_thread.start()
while winner is -2:
    if time.time() - last_time > 10.000:
        print("Player {} took too long to make a move".format(last_player ^ 1))
        winner = last_player
    else:
        time.sleep(0.005)

# Returns a datetime object containing the local date and time
dateTimeObj = datetime.datetime.now()
folder = "replays"
if len(sys.argv) > 5:
    folder = sys.argv[5]
if not os.path.exists(folder):
    os.mkdir(folder)
filename = "{}-{}-{}-{}-{}-{}-{}-{}-{}".format(
    dateTimeObj.year,
    dateTimeObj.month,
    dateTimeObj.day,
    dateTimeObj.hour,
    dateTimeObj.minute,
    dateTimeObj.second,
    dateTimeObj.microsecond,
    sys.argv[1],
    sys.argv[2]
)
filename = "last"
if len(sys.argv) > 6:
    filename = sys.argv[6]

print ("Player {} won!".format(winner))
with open(folder + "/" + filename, 'w') as f:
    f.write("{} {} {}\n".format(winner, r, c))
    f.write("{} vs {}\n".format(sys.argv[1], sys.argv[2]))
    for i in range(len(moves)):
        f.write("{} {}\n".format(moves[i][0], moves[i][1]))

# kill process
for i in range(2):
    if os.name is "posix" or True:
        proces[i].kill()
    else:
        subprocess.Popen("TASKKILL /F /PID {} /T".format(proces[i].pid))
