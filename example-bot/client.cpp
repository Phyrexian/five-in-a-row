#include <iostream>
#include <vector>
using namespace std;

// BOARD[i][j] is the character at position i, j
// empty cells have BOARD[i][j] equal to '.'
// ROWS and COLS tell you the number of rows and columns
// PR and PC tell you their last move, or [-1, -1] if the game has just started
// ME tells you if you are player 0 (X) or player 1 (O)
// MARK[ME] is therefore the character that belongs to you
// MARK[!ME] is the character that belongs to your opponent
// if you want to print stuff for debugging, use cerr instead of cout
// for example cerr << "Print IT" << endl;
vector<char> MARK{'X', 'O'};
vector<vector<char>> BOARD;
int ROWS, COLS, PR, PC, ME;

// this sends your move to the server
void submit_move(int row, int col) {
    BOARD[row][col] = MARK[ME];
    cout << row << " " << col << endl << flush;
}

void choose_move() {
    bool foundMove = false;
    for (int i = 0; i < ROWS && !foundMove; i++)
        for (int j = 0; j < COLS && !foundMove; j++)
            if (BOARD[i][j] == '.') {
                submit_move(i, j);
                foundMove = true;
            }
}

int main() {
    cin >> ROWS >> COLS >> ME;
    BOARD.assign(ROWS, vector<char>(COLS, '.'));
    while (cin >> PR >> PC) {
        if (PR != -1 && PC != -1)
            BOARD[PR][PC] = MARK[!ME];
        choose_move();
    }
}