#include <iostream>
#include <vector>
#include <time.h>
using namespace std;

vector<char> MARK{'X', 'O'};
vector<vector<char>> BOARD;
int ROWS, COLS, PR, PC, ME;

void submit_move(int row, int col) {
    BOARD[row][col] = MARK[ME];
    cout << row << " " << col << endl << flush;
}

void choose_move() {
    vector<pair<int, int>> moves;
    for (int i = 0; i < ROWS; i++)
        for (int j = 0; j < COLS; j++)
            if (BOARD[i][j] == '.')
                moves.push_back({i, j});
    int myMove = rand()%moves.size();
    submit_move(moves[myMove].first, moves[myMove].second);
}

int main() {
    srand(time(0));
    cin >> ROWS >> COLS >> ME;
    BOARD.assign(ROWS, vector<char>(COLS, '.'));
    while (cin >> PR >> PC) {
        if (PR != -1 && PC != -1)
            BOARD[PR][PC] = MARK[!ME];
        choose_move();
    }
}