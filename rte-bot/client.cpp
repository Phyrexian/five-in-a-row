#include <iostream>
#include <vector>
using namespace std;

vector<char> MARK{'X', 'O'};
vector<vector<char>> BOARD;
int ROWS, COLS, PR, PC, ME;

void submit_move(int row, int col) {
    BOARD[row][col] = MARK[ME];
    cout << row << " " << col << endl << flush;
}

void choose_move() {
    
}

int main() {
    cin >> ROWS >> COLS >> ME;
    BOARD.assign(ROWS, vector<char>(COLS, '.'));
    while (cin >> PR >> PC) {
        if (PR != -1 && PC != -1)
            BOARD[PR][PC] = MARK[!ME];
        exit(-1);
        // choose_move();
    }
}