import os

players = [ "random-bot", "example-bot" ]
score = [0] * len(players)
rounds, match = 1, 0
for k in range(rounds):
    for i in range(len(players)):
        for j in range(len(players)):
            if i is j:
                continue
            os.system("python server.py {} {} 25 25 tourney {}-{}-vs-{}".format(players[i], players[j], match, players[i], players[j]))
            with open("tourney/{}-{}-vs-{}".format(match, players[i], players[j]), 'r') as f:
                winner, _, _ = [ int(x) for x in f.readline().split(' ') ]
                if winner is 0:
                    score[i] += 1
                    score[j] -= 1
                elif winner is 1:
                    score[i] -= 1
                    score[j] += 1
            match += 1
print("--------------------------")
for i in range(len(players)):
    print(players[i], score[i])