#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

vector<char> MARK{'X', 'O'};
vector<vector<char>> BOARD;

int ROWS, COLS, PR, PC, ME;
int dr[] = { -1, -1, -1, 0, 1, 1, 1, 0 };
int dc[] = { -1, 0, 1, 1, 1, 0, -1, -1 };
const double INF = 1e9;

int winner () {
    for (int type = 0; type < 2; type++)
        for (int i = 0; i < ROWS; i++)
            for (int j = 0; j < COLS; j++)
                for (int k = 0; k < 4; k++)
                    for (int l = 0; l < 5; l++) {
                        int nr = i + dr[k] * l;
                        int nc = j + dc[k] * l;
                        if (nr < 0 || nr >= ROWS
                        || nc < 0 || nc >= COLS
                        || BOARD[nr][nc] != MARK[type])
                            break;
                        if (l == 4)
                            return type;
                    }
    return -1;
}

vector<pair<int, int>> findMoves () {
    vector<vector<bool>> b(ROWS, vector<bool>(COLS));
    for (int i = 0; i < ROWS; i++)
        for (int j = 0; j < COLS; j++)
            if (BOARD[i][j] != '.')
                for (int k = 0; k < 8; k++) {
                    int nr = i + dr[k];
                    int nc = j + dc[k];
                    if (nr >= 0 && nr < ROWS && nc >= 0 && nc < COLS
                    && BOARD[nr][nc] == '.')
                        b[nr][nc] = true;
                }
    vector<pair<int, int>> res;
    for (int i = 0; i < ROWS; i++)
        for (int j = 0; j < COLS; j++)
            if (b[i][j])
                res.push_back(make_pair(i, j));
    return res;
}

int calcDepth() {
    vector<pair<int, int>> moves = findMoves();
    return log(5000)/log(moves.size());
}

double evaluate () {
    vector<double> score{0, 0};
    for (int type = 0; type < 2; type++) {
        vector<int> open{0, 0, 0, 0, 0};
        vector<int> halfOpen{0, 0, 0, 0, 0};
        for (int i = 0; i < ROWS; i++)
            for (int j = 0; j < COLS; j++)
                for (int k = 0; k < 4; k++) {
                    int closed = 0;
                    int pr = i - dr[k];
                    int pc = j - dc[k];
                    if (pr >= 0 || pr < ROWS
                    || pc >= 0 || pc < COLS
                    || BOARD[pr][pc] == MARK[!type])
                        closed++;
                    else if (BOARD[pr][pc] == MARK[type])
                        continue;
                    
                    int mine = 0;
                    int nr = i + mine * dr[k];
                    int nc = j + mine * dc[k];
                    while (nr >= 0 && nr < ROWS
                    && nc >= 0 && nc < COLS
                    && BOARD[nr][nc] == MARK[type]) {
                        mine++;
                        nr = i + mine * dr[k];
                        nc = j + mine * dc[k];
                    }
                    
                    if (nr < 0 || nr >= ROWS
                    || nc < 0 || nc >= COLS
                    || BOARD[nr][nc] == MARK[!type])
                        closed++;
                    else if (BOARD[nr][nc] == MARK[type])
                        continue;
                    
                    if (closed == 2)
                        continue;
                    else if (closed == 1)
                        halfOpen[mine]++;
                    else
                         open[mine]++;
                }
        double openWeights[] = { 0, 1, 2, 20, 200 };
        double halfOpenWeights[] = { 0, 0.5, 1, 10, 100 };
        for (int i = 0; i < 5; i++) {
            score[type] += openWeights[i] * open[i];
            score[type] += halfOpenWeights[i] * halfOpen[i];
        }
    }
    return score[0] - score[1];
}

pair<double, int> miniMax (int player, int maxDepth, int depth = 0, double alpha=-INF, double beta=INF) {
    int hasWinner = winner();
    if (hasWinner != -1)
        return {(hasWinner == 0 ? INF : -INF), -1};
    vector<pair<int, int>> moves = findMoves();
    if (moves.empty())
        return {0, -1};
    if (depth == maxDepth)
        return {evaluate(), -1};
    if (player == 0) {
        int bestMove = -1;
        double bestScore = -INF;
        for (int i = 0; i < moves.size(); i++) {
            BOARD[moves[i].first][moves[i].second] = MARK[player];
            double moveScore = miniMax(!player, maxDepth, depth + 1, alpha, beta).first;
            BOARD[moves[i].first][moves[i].second] = '.';
            if (moveScore > bestScore)
                bestMove = i, bestScore = moveScore;
            alpha = max(alpha, bestScore);
            if (alpha >= beta)
                break;
        }
        return { bestScore, bestMove };
    } else {
        int bestMove = -1;
        double bestScore = INF;
        for (int i = 0; i < moves.size(); i++) {
            BOARD[moves[i].first][moves[i].second] = MARK[player];
            double moveScore = miniMax(!player, maxDepth, depth + 1, alpha, beta).first;
            BOARD[moves[i].first][moves[i].second] = '.';
            if (moveScore < bestScore)
                bestMove = i, bestScore = moveScore;
            beta = min(beta, bestScore);
            if (alpha >= beta)
                break;
        }
        return { bestScore, bestMove };
    }
}

void submit_move(int row, int col) {
    BOARD[row][col] = MARK[ME];
    cout << row << " " << col << endl << flush;
}

void choose_move() {
    if (PR == -1 && PC == -1)
        submit_move(ROWS/2, COLS/2);
    else {
        int depth = calcDepth();
        cerr << depth << endl;
        vector<pair<int, int>> moves = findMoves();
        int bestMove = miniMax(ME, depth).second;
        submit_move(moves[bestMove].first, moves[bestMove].second);
    }
}

int main() {
    cin >> ROWS >> COLS >> ME;
    BOARD.assign(ROWS, vector<char>(COLS, '.'));
    while (cin >> PR >> PC) {
        if (PR != -1 && PC != -1)
            BOARD[PR][PC] = MARK[!ME];
        choose_move();
    }
}