WRITING YOUR BOT
To write your bot, you can simply edit the default client.cpp.
You can put your code in the choose_move() function.

If you want to print things for debugging purposes, use cerr,
not cout. cout and cin are used to send and recieve messages
from the server.
Example: cerr << "Print IT" << endl;


PLAYING MATCHES
[] = optional arguement
command: python server.py bot0_folder bot1_folder [rows=15] [cols=15] [replay_folder=replays] [replay_name=currentTime()]
This simulates a match between bot0 and bot1. The resulting
replay is saved in the replay folder unless specified otherwise.
bot0 always plays first, and plays with X.
bot1 always plays second, and plays with O.

WATCHING REPLAYS
command: python observer.py path_to_replay
If you want to speed up the replay, you can edit the duration
of the pygame.time.delay(). To close the window, you can use ESC
or CTRL + W or ALT + F4.

SUBMITTING YOUR BOT & SCORING
Unlike the midnight challenge, this contest is not scored
throughout the whole duration, but is instead only scored at the
end. You can submit your bot by bringing it to me on a USB stick,
or by sending it to me in a zip file (through messanger).
You can do this throughout the contest. I will sometimes simulate
tournaments so you have feedback on how well your bot is playing.
The bot should be presented as a folder with your team's name,
and inside the folder there should be a client.cpp file and a
Makefile. Your folder can contain other files as well, if for
some reason you wish to use extra cpp or header files, or you
to have a text file with some constants or whatever.

Also, PLEASE AVOID PUTTING SPACES IN THE NAME OF THE FOLDER.

SIMULATING TOURNAMENTS
command: tourney.py
You can use tourney.py to simulate every pair of matches between
a list of competing bots. You need to specify which bots are playing
by editing the tourney.py folder. The replays from the tournament
are stored in the tourney folder and are numbered from 0.
I will use this to simulate the tournament when all the competition
is over, you don't really need to worry about this unless you
want to create your own mini tournaments during the contest.

SETTING UP ON WINDOWS:
You need python, make and g++ to be able to run server.py.
Also, you need pygame in order to run observer.py.
Downloading Python should be fairly trivial if you don't yet have
it, you simply google download python and download it from the
official python site. Once you have python, you can install
pygame by typing in terminal "python -m pip install pygame".
Next, you need to be able to run g++ from cmd. Open cmd and try typing
g++. If it complains that it doesn't recognise g++, you need to
add it to path. If you have code::blocks installed, you should
find g++ in C:\Program Files\CodeBlocks\MingW\bin .
For make, search "make for windows" and you should find gnuwin32
make, install it and also add it to path
(C:\Program Files (x86)\GnuWin32\bin)

QUESTIONS:
If you have any problems you can either find me and I can help you,
or you can just message me on messenger.